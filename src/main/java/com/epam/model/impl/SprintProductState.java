package com.epam.model.impl;

import com.epam.model.Product;
import com.epam.model.State;
import com.epam.model.StateTypes;

public class SprintProductState implements State {

    @Override
    public void addProductProduction(Product product) {
        System.out.println("Product add to Production - " + product.getName());
        product.setState(new ProductionProductState());
        product.setStateTypes(StateTypes.InPRODUCTION);
    }

    @Override
    public void addProductBlocked(Product product) {
        System.out.println("Product add to Blocked - " + product.getName());
        product.setState(new BlockedProductState());
        product.setStateTypes(StateTypes.InBLOCKED);
    }
}
