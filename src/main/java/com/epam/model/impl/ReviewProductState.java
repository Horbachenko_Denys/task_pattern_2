package com.epam.model.impl;

import com.epam.model.Product;
import com.epam.model.State;
import com.epam.model.StateTypes;

public class ReviewProductState implements State {

    @Override
    public void addProductSprint(Product product) {
        System.out.println("Product add to Sprint - " + product.getName());
        product.setState(new SprintProductState());
        product.setStateTypes(StateTypes.InSPRINT);
    }

    @Override
    public void addProductProduction(Product product) {
        System.out.println("Product add to Production - " + product.getName());
        product.setState(new ProductionProductState());
        product.setStateTypes(StateTypes.InPRODUCTION);
    }

    @Override
    public void addProductTest(Product product) {
        System.out.println("Product add to Test - " + product.getName());
        product.setState(new TestProductState());
        product.setStateTypes(StateTypes.InTEST);
    }

    @Override
    public void addProductBlocked(Product product) {
        System.out.println("Product add to Blocked - " + product.getName());
        product.setState(new BlockedProductState());
        product.setStateTypes(StateTypes.InBLOCKED);
    }
}
