package com.epam.model.impl;

import com.epam.model.Product;
import com.epam.model.State;
import com.epam.model.StateTypes;

public class ProductionProductState implements State {

    @Override
    public void addProductReview(Product product) {
        System.out.println("Product add to Review - " + product.getName());
        product.setState(new ReviewProductState());
        product.setStateTypes(StateTypes.InREVIEW);
    }
}
