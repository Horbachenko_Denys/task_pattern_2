package com.epam.model.impl;

import com.epam.model.Product;
import com.epam.model.State;
import com.epam.model.StateTypes;

public class BacklogProductState implements State {

    @Override
    public void addProductBacklog(Product product) {
        System.out.println("Product add to Backlog - " + product.getName());
        product.setState(new BacklogProductState());
        product.setStateTypes(StateTypes.InBACKLOG);
    }

    @Override
    public void addProductSprint(Product product) {
        System.out.println("Product add to Sprint - " + product.getName());
        product.setState(new SprintProductState());
        product.setStateTypes(StateTypes.InSPRINT);
    }

    @Override
    public void addProductBlocked(Product product) {
        System.out.println("Product add to Blocked - " + product.getName());
        product.setState(new BlockedProductState());
        product.setStateTypes(StateTypes.InBLOCKED);
    }
}
