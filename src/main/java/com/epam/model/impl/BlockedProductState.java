package com.epam.model.impl;

import com.epam.model.Product;
import com.epam.model.State;
import com.epam.model.StateTypes;

public class BlockedProductState implements State {

    @Override
    public void addProductBacklog(Product product) {
        System.out.println("Product add to Backlog - " + product.getName());
        product.setState(new BacklogProductState());
        product.setStateTypes(StateTypes.InBACKLOG);
    }

    @Override
    public void addProductSprint(Product product) {
        System.out.println("Product add to Sprint - " + product.getName());
        product.setState(new SprintProductState());
        product.setStateTypes(StateTypes.InSPRINT);
    }

    @Override
    public void addProductProduction(Product product) {
        System.out.println("Product add to Production - " + product.getName());
        product.setState(new ProductionProductState());
        product.setStateTypes(StateTypes.InPRODUCTION);
    }

    @Override
    public void addProductReview(Product product) {
        System.out.println("Product add to Review - " + product.getName());
        product.setState(new ReviewProductState());
        product.setStateTypes(StateTypes.InREVIEW);
    }

    @Override
    public void addProductTest(Product product) {
        System.out.println("Product add to Test - " + product.getName());
        product.setState(new TestProductState());
        product.setStateTypes(StateTypes.InTEST);
    }
}
