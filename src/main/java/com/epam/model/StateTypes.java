package com.epam.model;

public enum StateTypes {
InBACKLOG, InSPRINT, InPRODUCTION, InREVIEW, InTEST, InDONE, InBLOCKED
}
