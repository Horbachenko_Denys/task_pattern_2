package com.epam.model;

import com.epam.view.MyView;

public interface State {
    default void addProductBacklog(Product product) {
        MyView.logger.info("Add product in backlog is not allowed");
    }

    default void addProductSprint(Product product) {
        MyView.logger.info("Add product in sprint is not allowed");
    }

    default void addProductProduction(Product product) {
        MyView.logger.info("Add product in production is not allowed");
    }

    default void addProductReview(Product product) {
        MyView.logger.info("Add product in review is not allowed");
    }

    default void addProductTest(Product product) {
        MyView.logger.info("Add product in test is not allowed");
    }

    default void addProductDone(Product product) {
        MyView.logger.info("Add product in done section is not allowed");
    }

    default void addProductBlocked(Product product) {
        MyView.logger.info("Add product in blocked section is not allowed");
    }
}
