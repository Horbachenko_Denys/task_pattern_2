package com.epam.model;

public class Product {
    private String name;
    private static int counter = 0;
    private int id = 0;
    private State state;
    private StateTypes stateTypes;

    public TeamMember getTeamMember() {
        return teamMember;
    }

    private TeamMember teamMember = null;

    public void setTeamMember(TeamMember teamMember) {
        this.teamMember = teamMember;
    }

    public Product(String name, State state) {
        this.name = name;
        counter++;
        this.id = counter;
        this.state = state;
    }

    public Product(String name, State state, TeamMember member) {
        this.name = name;
        counter++;
        this.id = counter;
        this.state = state;
        this.teamMember = member;

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public State getState() {
        return state;
    }

    public StateTypes getStateTypes() {
        return stateTypes;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setStateTypes(StateTypes stateTypes) {
        this.stateTypes = stateTypes;
    }


}
