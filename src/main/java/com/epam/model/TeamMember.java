package com.epam.model;

public class TeamMember {
    private String name;
    private static int counter = 0;
    private int id = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        TeamMember.counter = counter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TeamMember(String name) {
        this.name = name;
        counter++;
        this.id = counter;
    }
}
