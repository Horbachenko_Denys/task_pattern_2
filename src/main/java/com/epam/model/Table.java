package com.epam.model;

import com.epam.model.impl.BacklogProductState;
import com.epam.view.MyView;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Table {
    private List<Product> products;
    private List<TeamMember> members;
    private String error = "There is no tasks!";


    public Table() {
        products = new ArrayList<>();
        members = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return products;
    }

    public List<TeamMember> getMembers() {
        return members;
    }

    public void addProductBacklog(String name) {
        State state = new BacklogProductState();
        Product newProduct = new Product(name, null);
        products.add(newProduct);
        state.addProductBacklog(newProduct);
    }

    public void addProductSprint() {
        if (products.isEmpty()) {
            MyView.logger.info(error);
        } else {
            int id = getProductId();
            Product product = products.get(id);
            product.getState().addProductSprint(product);
        }
    }

    public void addProductProduction() {
        if (products.isEmpty()) {
            MyView.logger.info(error);
        } else {
            int id = getProductId();
            Product product = products.get(id);
            product.getState().addProductProduction(product);
        }
    }

    public void addProductReview() {
        if (products.isEmpty()) {
            MyView.logger.info(error);
        } else {
            int id = getProductId();
            Product product = products.get(id);
            product.getState().addProductReview(product);
        }
    }

    public void addProductTest() {
        if (products.isEmpty()) {
            MyView.logger.info(error);
        } else {
            int id = getProductId();
            Product product = products.get(id);
            product.getState().addProductTest(product);
        }
    }

    public void addProductDone() {
        if (products.isEmpty()) {
            MyView.logger.info(error);
        } else {
            int id = getProductId();
            Product product = products.get(id);
            product.getState().addProductDone(product);
        }
    }

    public void addProductBlocked() {
        if (products.isEmpty()) {
            MyView.logger.info(error);
        } else {
            int id = getProductId();
            Product product = products.get(id);
            product.getState().addProductBlocked(product);
        }
    }

    private int getProductId() {
        Scanner input = new Scanner(System.in);
        MyView.logger.info("Enter task Id...");
        int id = Integer.parseInt(input.nextLine());
        id--;
        return id;
    }

    public void addMember(String member) {
        members.add(new TeamMember(member));
        MyView.logger.info("New team member had been added.");
    }

    public void assignProduct(int memberId, int productId) {
        for (Product product : products) {
            if (product.getId() == productId) {
                TeamMember member = getMember(memberId);
                if (member != null) {
                    product.setTeamMember(member);
                    MyView.logger.info("Task {}, assigned to memder  {}", product.getName(), member.getName());
                } else {
                    MyView.logger.info("No members with such id.");
                }

            }
        }
    }

    private TeamMember getMember(int id) {
        for (TeamMember member : members) {
            if (member.getId() == id)
                return member;
        }
        return null;
    }
}

