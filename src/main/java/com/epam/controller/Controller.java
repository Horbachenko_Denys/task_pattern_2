package com.epam.controller;

public interface Controller {

    void addProductBacklog(String name);

    void addProductSprint();

    void addProductProduction();

    void addProductReview();

    void addProductTest();

    void addProductDone();

    void addProductBlocked();

    void showAllProduct();

    void addMember(String member);

    void showAllMembers();

    void assignProduct(int memberId, int taskId);

    void showAllTaskMembers();
}
