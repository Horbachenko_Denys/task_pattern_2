package com.epam.controller;

import com.epam.model.Product;
import com.epam.model.State;
import com.epam.model.Table;
import com.epam.model.TeamMember;
import com.epam.view.MyView;

public class ControllerImpl implements Controller {
    private State state;
    private Table table = new Table();

    @Override
    public void addProductBacklog(String name) {
        table.addProductBacklog(name);
    }

    @Override
    public void addProductSprint() {
        table.addProductSprint();
    }

    @Override
    public void addProductProduction() {
        table.addProductProduction();
    }

    @Override
    public void addProductReview() {
        table.addProductReview();
    }

    @Override
    public void addProductTest() {
        table.addProductTest();
    }

    @Override
    public void addProductDone() {
        table.addProductDone();
    }

    @Override
    public void addProductBlocked() {
        table.addProductBlocked();
    }

    @Override
    public void showAllProduct() {
        for (Product product : table.getProducts()) {
            MyView.logger.info("Product {}, id: {}, status is: {}", product.getName(), product.getId(), product.getStateTypes());
        }
    }

    @Override
    public void addMember(String member) {
        table.addMember(member);
    }

    @Override
    public void showAllMembers() {
        for (TeamMember member : table.getMembers()) {
            MyView.logger.info("Team member: {}, id: {}", member.getName(), member.getId());
        }
    }

    @Override
    public void assignProduct(int memberId, int taskId) {
        table.assignProduct(memberId, taskId);
    }

    @Override
    public void showAllTaskMembers() {
        for (Product product : table.getProducts()) {
            if (product.getTeamMember() != null) {
                MyView.logger.info("Product {}, id: {}, status is: {}, assigned to: {}, id: {}",
                        product.getName(), product.getId(), product.getStateTypes(),
                        product.getTeamMember().getName(), product.getTeamMember().getId());
            } else {
                MyView.logger.info("Product {}, id: {}, status is: {}, assigned to no one",
                        product.getName(), product.getId(), product.getStateTypes());
            }
        }
    }
}
