package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class MyView {
    public static final Logger logger = LogManager.getLogger(MyView.class);
    private static Scanner input = new Scanner(System.in);
    private String keyMenu;
    private Controller controller;

    public MyView() {
        controller = new ControllerImpl();
        do {
            showMenu();
        } while (!keyMenu.equals("Q"));
    }

    private void showMenu() {
        logger.info("                             ===== Scrum menu: =====\n" +
                "   1  - Add Product in Backlog           " + "   2  - Move Product in Sprint Backlog\n" +
                "   3  - Move Product into Production     " + "   4  - Move Product into Review\n" +
                "   5  - Move Product into Test           " + "   6  - Move Product in Done section\n" +
                "   7  - Move Product into Blocked section" + "   8  - Show all Product\n" +
                "   9  - Add team member                  " + "   10 - Show all team members\n" +
                "   11 - Assign Task                      " + "   12 - Show tasks by team members\n" +
                "   Q  - exit\n" +
                "   Please, select menu point.");
        keyMenu = input.nextLine().toUpperCase();
        try {
            switch (keyMenu) {
                case "1":
                    String name;
                    logger.info("Enter name of product");
                    name = input.nextLine();
                    controller.addProductBacklog(name);
                    break;
                case "2":
                    controller.addProductSprint();
                    break;
                case "3":
                    controller.addProductProduction();
                    break;
                case "4":
                    controller.addProductReview();
                    break;
                case "5":
                    controller.addProductTest();
                    break;
                case "6":
                    controller.addProductDone();
                    break;
                case "7":
                    controller.addProductBlocked();
                    break;
                case "8":
                    controller.showAllProduct();
                    break;
                case "9":
                    String member;
                    logger.info("Enter team members name:");
                    member = input.nextLine();
                    controller.addMember(member);
                    break;
                case "10":
                    controller.showAllMembers();
                    break;
                case "11":
                    controller.showAllMembers();
                    logger.info("Choose team member id:");
                    int memberId = input.nextInt();
                    controller.showAllProduct();
                    logger.info("Choose task id:");
                    int taskId = input.nextInt();
                    controller.assignProduct(memberId, taskId);
                    break;
                case "12":
                    controller.showAllTaskMembers();
                    break;
                case "Q":
                    return;
                default:
                    logger.info("Wrong input, try again");
                    showMenu();
            }
        } catch (Exception e) {
            logger.error("Error in menu");
        }
    }
}
